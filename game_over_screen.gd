extends CanvasLayer

@onready var score_label = $PanelContainer/MarginContainer/Rows/Score

func _ready():
	score_label.set_text("Score: " + str(GlobalVariables.score))

func _on_restart_button_pressed():
	get_tree().change_scene_to_file("res://first_map.tscn")
	GlobalVariables.score = 0

func _on_quit_button_pressed():
	get_tree().quit()
