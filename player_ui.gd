extends Control

@onready var score = $PanelContainer/MarginContainer/Score
@onready var lifebar = $PanelContainer/MarginContainer/LifeBar
@onready var life = $PanelContainer/MarginContainer/LifeBar/Life

func _process(_delta):
	score.set_text("Score: " + str(GlobalVariables.score))
	lifebar.set_value_no_signal(GlobalVariables.player_health)
	life.set_text(str(GlobalVariables.player_health))
