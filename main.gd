extends Node3D

const BASE_SPAWN_COOLDOWN = 1

@export var base_enemy: PackedScene
@export var radius: float
var timer = 0
var spawn_cooldown = BASE_SPAWN_COOLDOWN
var spawn_acceleration = 0

func _ready():
	DisplayServer.window_set_mode(DisplayServer.WINDOW_MODE_FULLSCREEN)

func _physics_process(delta):
	timer += delta
	spawn_cooldown -= delta
	if(spawn_cooldown <= 0):
		var enemy = base_enemy.instantiate()
		add_child(enemy)
		set_stats(enemy)
		spawn_acceleration += 0.002
		spawn_cooldown = BASE_SPAWN_COOLDOWN - spawn_acceleration

func set_stats(enemy):
	var random_position = Vector3(randf_range(-radius, radius), 0, randf_range(-radius, radius))
	enemy.set_position(random_position)
	enemy.set_health(enemy.BASE_HEALTH + timer / 10)
	enemy.set_speed(enemy.BASE_SPEED * randf_range(0.7, 1.5))

func handle_game_over():
	Input.set_mouse_mode(Input.MOUSE_MODE_VISIBLE)
	get_tree().change_scene_to_file("res://game_over_screen.tscn")
