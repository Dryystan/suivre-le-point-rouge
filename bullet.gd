extends CharacterBody3D

const DAMAGE = 5
const SPEED = 150
var direction

func _physics_process(delta):
	var collision = move_and_collide(-direction.z * delta * SPEED)
	if collision:
		if collision.get_collider().is_in_group("enemies"):
			collision.get_collider().take_damage(DAMAGE)
		queue_free()

func set_direction(initial_direction):
	direction = initial_direction
