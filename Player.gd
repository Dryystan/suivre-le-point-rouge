extends CharacterBody3D

const WALK_SPEED = 5.0
const SPRINT_MULTIPLIER = 1.5
const JUMP_VELOCITY = 4.5
const SENSITIVITY = 0.002
const FOV = 90
const MIN_FOV = 40
const FOV_CHANGE = 2

# Get the gravity from the project settings to be synced with RigidBody nodes.
var gravity = ProjectSettings.get_setting("physics/3d/default_gravity")
var air_time = 0
var jumped = false
var health = GlobalVariables.MAX_PLAYER_HEALTH
var distance_from_dot = 0
var time_spent_under_dot = 0

@onready var head = $Head
@onready var camera = $Head/Camera3D
@onready var muzzle = $Head/Marker3D
@export var bullet: PackedScene

func _ready():
	Input.set_mouse_mode(Input.MOUSE_MODE_CAPTURED)

func _input(event):
	if event is InputEventMouseMotion:
		head.rotate_y(-event.relative.x * SENSITIVITY)
		camera.rotate_x(-event.relative.y * SENSITIVITY)
		camera.rotation.x = clamp(camera.rotation.x, deg_to_rad(-50), deg_to_rad(70))

func _process(_delta):
	GlobalVariables.update_player_location(position)

func _physics_process(delta):
	# Add the gravity.
	if not is_on_floor():
		air_time += delta
		if ( air_time > 0.2 ):
			jumped = true
		velocity.y -= gravity * delta
	else:
		air_time = 0
		jumped = false
	
	if(GlobalVariables.current_dot_location):
		distance_from_dot = position.distance_to(GlobalVariables.current_dot_location)
		if(distance_from_dot <= 15.0):
			time_spent_under_dot += delta
			if(time_spent_under_dot >= 2):
				update_health(health + 2)
				time_spent_under_dot = 0

	handle_movement_and_shooting()
	handle_jump()
	handle_FOV()

	move_and_slide()

func handle_movement_and_shooting():
	# Get the input direction and handle the movement/deceleration.
	# As good practice, you should replace UI actions with custom gameplay actions.
	var input_dir = Input.get_vector("left", "right", "forward", "backward")
	var direction = (head.transform.basis * Vector3(input_dir.x, 0, input_dir.y)).normalized()
	
	var speed = WALK_SPEED
	#if Input.is_action_pressed("sprint"):
		#speed = WALK_SPEED * SPRINT_MULTIPLIER
	#else:
		#speed = WALK_SPEED
	
	if direction:
		if is_on_floor():
			velocity.x = direction.x * speed
			velocity.z = direction.z * speed
		else:
			velocity.x = move_toward(velocity.x, direction.x * speed, speed / 30)
			velocity.z = move_toward(velocity.z, direction.z * speed, speed / 30)
	else:
		if is_on_floor():
			velocity.x = 0;
			velocity.z = 0;
		else:
			velocity.x = move_toward(velocity.x, 0, speed / 200)
			velocity.z = move_toward(velocity.z, 0, speed / 200)
	
	if Input.is_action_just_pressed("shoot"):
		shoot(head.transform.basis)

func handle_jump():
	# Handle Jump.
	if Input.is_action_just_pressed("jump"):
		if is_on_floor():
			velocity.y = JUMP_VELOCITY
			jumped = true
		if not is_on_floor() and not jumped:
			velocity.y = JUMP_VELOCITY
			jumped = true

func handle_FOV():
	if GlobalVariables.current_dot_location:
		var distance = self.position.distance_to(GlobalVariables.current_dot_location) - GlobalVariables.MAX_DISTANCE_FROM_POINT
		if distance > 0:
			var target_fov = FOV - FOV_CHANGE * distance
			camera.fov = clamp( target_fov, MIN_FOV, FOV)

func shoot(direction):
	var b = bullet.instantiate()
	owner.add_child(b)
	b.set_direction(direction)
	b.transform = muzzle.global_transform

func take_damage(amount):
	if(distance_from_dot > 10.0):
		amount += int((distance_from_dot - 10) / 2)
	update_health(health - amount)
	if health <= 0:
		get_tree().call_group("map", "handle_game_over")

func update_health(new_health):
	health = clamp(new_health, 0, GlobalVariables.MAX_PLAYER_HEALTH)
	GlobalVariables.set_player_health(health)
