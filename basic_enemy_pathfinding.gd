extends CharacterBody3D

const BASE_DAMAGE = 10
const BASE_SPEED = 5.5
const SCORE = 100
const BASE_HEALTH = 10
const ATTACK_SPEED = 1
var damage = BASE_DAMAGE
var health = BASE_HEALTH
var speed = BASE_SPEED

@onready var nav_agent = $NavigationAgent3D
@onready var mesh = $MeshInstance3D
@onready var dot_current_position

func _physics_process(delta):
	var current_location = global_transform.origin
	var next_location = nav_agent.get_next_path_position()
	var new_velocity = (next_location - current_location).normalized() * speed
	
	var material = mesh.get_active_material(0)
	if(nav_agent.distance_to_target() <= 2.0):
		material.set_emission_energy_multiplier( clamp(material.get_emission_energy_multiplier() + ATTACK_SPEED * delta, 0.1, 1.0) )
	else:
		material.set_emission_energy_multiplier( clamp(material.get_emission_energy_multiplier() - ATTACK_SPEED / 4.0 * delta, 0.1, 1.0) )
	if(material.get_emission_energy_multiplier() >= 1.0):
		attack()
	
	nav_agent.set_velocity(new_velocity)

func attack():
	get_tree().call_group("player", "take_damage", damage)
	mesh.get_active_material(0).set_emission_energy_multiplier( 0.1 )

func update_target_location(target_location):
	nav_agent.set_target_position(target_location)

func _on_navigation_agent_3d_velocity_computed(safe_velocity):
	velocity = velocity.move_toward(safe_velocity, 0.25)
	move_and_slide()

func set_speed(new_speed):
	speed = new_speed

func set_damage(new_damage):
	damage = new_damage

func set_health(new_health):
	health = new_health

func take_damage(taken_damage):
	health -= taken_damage
	if health <= 0:
		GlobalVariables.raise_score(SCORE)
		queue_free()
