extends Node

const MAX_DISTANCE_FROM_POINT = 15
const MAX_PLAYER_HEALTH = 100
var score = 0
var current_dot_location
var current_player_location
var player_health = MAX_PLAYER_HEALTH

func raise_score(score_added):
	score += int(score_added * current_player_location.distance_to(current_dot_location) / 3)

func set_player_health(health):
	player_health = health

func update_player_location(player_location):
	current_player_location = player_location

func update_dot_location(dot_location):
	current_dot_location = dot_location
